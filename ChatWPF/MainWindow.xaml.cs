﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ChatWPF
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly string _ipAddress = "224.1.2.3";
        private readonly int _port = 10001;

        private bool _isLogged = false;

        private UdpClient _udpClient;
        private int _ttl = 42;

        private byte[] _buffer;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void LogInOutButtonClick(object sender, RoutedEventArgs e)
        {
            if (!_isLogged)
            {
                _udpClient = new UdpClient(_port);
                _udpClient.JoinMulticastGroup(IPAddress.Parse(_ipAddress), _ttl);

                _isLogged = true;

                var task = new Task(new Action(() =>
                {
                    try
                    {
                        while (_isLogged)
                        {
                            IPEndPoint remoteEndPoint = null;
                            _buffer = _udpClient.Receive(ref remoteEndPoint);
                            string message = Encoding.Unicode.GetString(_buffer);

                            Dispatcher.BeginInvoke(new ThreadStart(() =>
                            {
                                chatTextBox.Text = $"{chatTextBox.Text}\r\n{DateTime.Now.ToShortTimeString()}:  {message}\r\n";
                            }));
                        }
                    }
                    catch (ObjectDisposedException)
                    {
                        if (!_isLogged)
                            return;
                    }
                    catch (SocketException)
                    {
                        return;
                    }
                }));
                task.Start();

                _buffer = Encoding.Unicode.GetBytes($"{userNameTextBox.Text} is logged in");
                _udpClient.Send(_buffer, _buffer.Length, _ipAddress, _port);

                logInOutButton.Content = "Logout";
            }
            else
            {
                Exit();

                logInOutButton.Content = "Login";
                
                userNameTextBox.Clear();
            }
        }

        private void SendButtonClick(object sender, RoutedEventArgs e)
        {
            if (_isLogged)
            {
                _buffer = Encoding.Unicode.GetBytes($"{userNameTextBox.Text}: {messageTextBox.Text}");
                _udpClient.Send(_buffer, _buffer.Length, _ipAddress, _port);

                messageTextBox.Clear();
            }
            else MessageBox.Show("Well, you can't");
        }

        private void Exit()
        {
            _buffer = Encoding.Unicode.GetBytes($"{userNameTextBox.Text} is logged out");
            _udpClient.Send(_buffer, _buffer.Length, _ipAddress, _port);

            _udpClient.DropMulticastGroup(IPAddress.Parse(_ipAddress));

            _isLogged = false;

            _udpClient.Close();
        }

        private void WindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (_isLogged)
                Exit();
        }
    }
}
